import { Express } from "express";
import { login } from "./controllers/login-controller";
import { create, currentUser, list, remove, update } from "./controllers/user-controller";
import { authenticate } from "./middlewares/authentication";
import { validateCreateData } from "./middlewares/validate-create-data";
import { validateLoginData } from "./middlewares/validate-login-data";
import { validateUpdateData } from "./middlewares/validate-update-data";

export const initializerRouter = (app: Express) => {
    app.post('/users', validateCreateData, create);
    app.post('/login', validateLoginData, login);
    app.get('/users', authenticate, list);
    app.get('/users/profile', authenticate, currentUser);
    app.patch('/users/:uuid', validateUpdateData, authenticate, update);
    app.delete('/users/:uuid', authenticate, remove);
}
