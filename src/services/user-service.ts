import bcrypt from 'bcrypt';
import { getRepository, getCustomRepository } from "typeorm";
import User from "../entities/user";

import UserRepository from "../repositories/userRepository";

export interface UserBody {
    name: string,
    email: string,
    password?: string,
    isAdm: boolean
}

export const checkUserExists = async (body: UserBody) => {
    const { email } = body;
    try {
        const userRepository = getCustomRepository(UserRepository);
        const user = await userRepository.findByEmail(email);
        if (user === undefined) {
            return false;
        }
        return true;
    } catch (error) {
        console.log(error);
    }
}

export const createUser = async (body: UserBody) => {
    const { name, email, password, isAdm } = body;
    try {
        const userRepository = getRepository(User);
        const user = userRepository.create({
            name,
            email,
            password,
            isAdm,
            createdOn: new Date(),
            updatedOn: new Date()
        });
        await userRepository.save(user);
        let userCopy: UserBody = { ...user };
        delete userCopy.password;
        return userCopy;
    } catch (error) {
        throw new Error();
    }
}

export const listUsers = async () => {
    const userRepository = getRepository(User);
    const users: UserBody[] = await userRepository.find(undefined);
    for (let user of users) {
        delete user.password;
    }
    return users;
}

export const checkIsAdm = async (id: string) => {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findById(id);
    if (!user) {
        return undefined;
    }
    return user.isAdm;
}

export const updateUser = async (userId: string, data: any) => {
    try {
        const userRepository = getCustomRepository(UserRepository);
        const user = await userRepository.findById(userId);
        if (data.password) {
            data.password = bcrypt.hashSync(data.password, 10);
        }
        data.updatedOn = new Date();
        return await userRepository.save({
            ...user, ...data
        });
    } catch (error) {
        return undefined;
    }
}

export const deleteUser = async (userId: string) => {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne(userId);
    if (!user) {
        return false;
    }
    await userRepository.remove([user]);
    return true;
}
