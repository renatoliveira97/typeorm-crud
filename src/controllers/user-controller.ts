import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import UserRepository from "../repositories/userRepository";
import { checkIsAdm, checkUserExists, createUser, deleteUser, listUsers, updateUser, UserBody } from "../services/user-service";

export const create = async (req: Request, res: Response) => {
    try {
        const userExists = await checkUserExists(req.body);
        if (!userExists) {
            const user = await createUser(req.body);
            return res.status(201).json(user);
        }
        return res.status(400).json({
            message: "Email already registered"
        })
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
}

export const list = async (req: Request, res: Response) => {
    if (!req.user) {
        return res.status(500).json({
            message: "Corrupted user"
        });
    }
    const isAdm = await checkIsAdm(req.user.id);
    if (!isAdm) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    const users = await listUsers();
    return res.status(200).json(users);
}

export const currentUser = async (req: Request, res: Response) => {
    const userRepository = getCustomRepository(UserRepository);
    if (!req.user) {
        return res.status(500).json({
            message: "Corrupted user"
        });
    }
    const user: UserBody | undefined = await userRepository.findById(req.user.id);
    if (!user) {
        return res.status(500).json({
            message: "Corrupted id"
        });
    }
    delete user.password;
    return res.status(200).json(user);
}

export const update = async (req: Request, res: Response) => {
    try {
        const { uuid } = req.params;
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const isAdm = await checkIsAdm(req.user.id);
        const isOwner = uuid === req.user.id;
        if (!isOwner && !isAdm) {
            return res.status(401).json({
                message: "Missing admin permissions"
            });
        }
        const updatedUser = await updateUser(uuid, req.body);
        if (!updatedUser) {
            return res.status(400).json({
                message: "User doesn't exists"
            });
        }
        delete updatedUser.password;
        return res.send(updatedUser);

    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
}

export const remove = async (req: Request, res: Response) => {
    try {
        const { uuid } = req.params;
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const isAdm = await checkIsAdm(req.user.id);
        const isOwner = uuid === req.user.id;
        if (!isOwner && !isAdm) {
            return res.status(401).json({
                message: "Missing admin permissions"
            });
        }
        const deleted = await deleteUser(uuid);
        if (!deleted) {
            return res.status(400).json({
                message: "User doesn't exists"
            });
        }
        return res.status(200).json({
            message: "User deleted with success"
        });
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }

}
