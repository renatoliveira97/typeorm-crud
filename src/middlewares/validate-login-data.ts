import { Request, Response, NextFunction } from "express";

export const validateLoginData = async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    try {
        if (!email) {
            return res.status(400).json({
                message: 'Missing parameter email'
            });
        }
        if (!password) {
            return res.status(400).json({
                message: 'Missing parameter password'
            });
        }
        next();
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        })
    }
}