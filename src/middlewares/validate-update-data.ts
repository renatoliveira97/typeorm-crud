import { Request, Response, NextFunction } from "express";

export const validateUpdateData = async (req: Request, res: Response, next: NextFunction) => {
    const { isAdm, createdOn, updatedOn } = req.body;
    try {
        if (isAdm) {
            return res.status(400).json({
                message: "isAdm can't be updated"
            });
        }
        if (createdOn) {
            return res.status(400).json({
                message: "createdOn can't be updated"
            });
        }
        if (updatedOn) {
            return res.status(400).json({
                message: "updatedOn can't be updated"
            });
        }
        next();
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        })
    }
}
