import {MigrationInterface, QueryRunner} from "typeorm";

export class UserClassUpdate1644173815385 implements MigrationInterface {
    name = 'UserClassUpdate1644173815385'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "createdOn" TIMESTAMP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "createdOn"`);
    }

}
